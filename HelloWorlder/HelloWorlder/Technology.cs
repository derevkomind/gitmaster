using System;

namespace HelloWorlder
{
    public class Technology<T> : ITechIn<int>, ITechOut<int>
    {
        public string Super(int data)
        {
            return data.ToString();
        }
    
        public int Mega(string data)
        {
            return int.Parse(data);
        }
    }
}